import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import { Navbar } from './components/navbar';
import { HomeDisplay } from './components/home_display';
import { Loading } from './components/loading';

import shortHash from 'short-hash'

import "./container.css"
import { ViewVideo } from './components/video';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            error: null
        }
    }

    render() {
        console.log(this.state.data)
        return (
            <>
                <Navbar />
                <div className="aui container">
                    <p>THIS IS A PROOF OF CONCEPT. PLEASE DON'T SUE ME!</p>
                </div>
            
                <Route path="/" exact render={() => <HomeDisplay />} />
                <Route path="/video/:video_id" exact render={(info) => <ViewVideo video={info.match.params.video_id} />} />
            </>
        );
    }
}

export default App;
