import React from "react";
import { Typography, Card } from "rmwc";
import { VideoPlayer } from "./video_player";

export class ViewVideo extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            video_info: null
        }
    }

    componentDidMount() {
        fetch(`/api/dataByID?id=${this.props.video}`)
            .then((response) => response.json())
            .then((data) => this.setState({ video_info: data }))
            .catch((error) => {
                console.log(error)
                this.setState({ error: error })
            })
    }

    render() {
        const { video_info } = this.state
        return (
            <div className="aui container">
                {video_info ? (
                    <Card key={video_info.hash} style={{ margin: '1rem 0 1rem 0' }}>
                        <VideoPlayer controls={true} aspectRatio="16:9" sources={[{
                            src: video_info.video_hd ? video_info.video_hd : video_info.video_sd,
                            type: 'video/mp4'
                        }]} style={{ width: "100%", height: "100%" }} />
                        <div style={{ padding: '0 1rem 1rem 1rem' }}>
                            <Typography use="headline6" tag="h2">
                                {video_info.title}
                            </Typography>
                            <Typography
                                use="subtitle2"
                                tag="h3"
                                theme="text-secondary-on-background"
                                style={{ marginTop: '-1rem' }}
                            >
                                {video_info.broadcaster}
                            </Typography>
                            <Typography use="body1" tag="div" theme="text-secondary-on-background">
                                {video_info.description}{(video_info.description.substring(video_info.description.length - 1) === ".") ? "" : "..."}
                            </Typography>
                        </div>
                    </Card>

                ) : <></>}
            </div>
        )
    }
}