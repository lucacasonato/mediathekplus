import React from "react";

import { Typography } from "@rmwc/typography";

import { CircularProgress } from '@rmwc/circular-progress';
import '@rmwc/circular-progress/circular-progress.css';

export class Loading extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            location: ""
        }
    }

    render() {
        return (
            <div className="aui container" style={{ margin: "20px 0", textAlign: "center" }}>
                <CircularProgress size="72" style={{ margin: "20px 0 0 0" }} />
                <p><Typography use="headline4">Loading library...</Typography></p>
            </div>
        )
    }
}