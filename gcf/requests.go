package gcf

import (
	"encoding/json"
	"net/http"
	"strconv"
)

var Data []*Video
var Pages [][]*Video
var PagesJSON [][]byte
var DataMapJSON = make(map[string][]byte)
var QueryCache = make(map[string][][]*Video)
var QueryCacheJSON = make(map[string][][]byte)

func init() {
	Data = sortVideos(get())
	var err error
	for _, d := range Data {
		DataMapJSON[d.ID], err = json.MarshalIndent(d, "", "  ")
		if err != nil {
			panic(err)
		}
	}

	Pages = paginate(Data, 10)

	PagesJSON = make([][]byte, len(Pages))

	for i, page := range Pages {
		PagesJSON[i], err = json.MarshalIndent(page, "", "  ")
		if err != nil {
			panic(err)
		}
	}
}

//PaginatedData sends back the requested page
func PaginatedData(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	page := r.Form.Get("page")
	if page == "" {
		http.Error(w, "page not specified", http.StatusBadRequest)
		return
	}

	pageInt, err := strconv.Atoi(page)
	if err != nil {
		http.Error(w, "page not valid int", http.StatusBadRequest)
		return
	}

	if PagesJSON[pageInt] == nil {
		http.Error(w, "page not found", http.StatusNotFound)
		return
	}

	w.Header().Add("content-type", "application/json")
	w.Write(PagesJSON[pageInt])
}

//PaginatedSearch sends back the requested queried page
func PaginatedSearch(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	page := r.Form.Get("page")
	if page == "" {
		http.Error(w, "page not specified", http.StatusBadRequest)
		return
	}

	pageInt, err := strconv.Atoi(page)
	if err != nil {
		http.Error(w, "page not valid int", http.StatusBadRequest)
		return
	}

	query := r.Form.Get("query")
	if query == "" {
		http.Error(w, "query not specified", http.StatusBadRequest)
		return
	}

	if QueryCacheJSON[query] == nil {
		QueryCache[query] = paginate(search(Data, query), 10)
		QueryCacheJSON[query] = make([][]byte, len(QueryCache[query]))

		var err error
		for i, page := range QueryCache[query] {
			QueryCacheJSON[query][i], err = json.MarshalIndent(page, "", "  ")
			if err != nil {
				panic(err)
			}
		}

	}

	if QueryCacheJSON[query][pageInt] == nil {
		http.Error(w, "page not found", http.StatusNotFound)
		return
	}

	w.Header().Add("content-type", "application/json")
	w.Write(QueryCacheJSON[query][pageInt])
}

//DataByID sends back the requested data
func DataByID(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	id := r.Form.Get("id")
	if id == "" {
		http.Error(w, "id not specified", http.StatusBadRequest)
		return
	}

	if DataMapJSON[id] == nil {
		http.Error(w, "data not found", http.StatusNotFound)
		return
	}

	w.Header().Add("content-type", "application/json")
	w.Write(DataMapJSON[id])
}
