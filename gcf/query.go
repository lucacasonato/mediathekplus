package gcf

import (
	"strings"
)

func search(data []*Video, query string) (videos []*Video) {
	videos = make([]*Video, 0)
	for _, video := range data {
		q := strings.ToLower(query)
		if strings.Contains(strings.ToLower(video.Broadcaster), q) ||
			strings.Contains(strings.ToLower(video.Title), q) ||
			strings.Contains(strings.ToLower(video.Series), q) ||
			strings.Contains(strings.ToLower(video.Description), q) ||
			strings.Contains(strings.ToLower(video.Date), q) {
			videos = append(videos, video)
		}
	}

	return
}

func SearchExport(query string) []*Video {
	return search(Data, query)
}
