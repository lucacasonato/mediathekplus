package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"

	gcf "gitlab.com/creativeguy2013/mediathekplus/gcf"
)

func main() {
	fmt.Println("--")
	start := time.Now()
	d, _ := json.MarshalIndent(gcf.PaginateExport(gcf.SearchExport("notruf"), 10), "", "  ")
	ioutil.WriteFile("d.txt", d, 0666)
	fmt.Println(time.Since(start).String())
}
